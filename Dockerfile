FROM openjdk:8
ADD target/api-gateway.jar api-gateway.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "api-gateway.jar"]
CMD dockerize -wait tcp://registry-service:8761 -timeout 60m